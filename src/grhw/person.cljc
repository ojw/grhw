(ns grhw.person
  (:require [clojure.spec.alpha :as spec]

            [clj-time.coerce :as coerce]
            [clj-time.format :as format]))

;;;;;;;;;;;;;;;;;
;;; The model ;;;
;;;;;;;;;;;;;;;;;

;;; This is currently used only for generation during testing,
;;; but could be used for validation at system boundaries,
;;; used as part of function specs, etc.

(spec/def ::last-name (spec/and string? (comp not empty?)))


(spec/def ::first-name (spec/and string? (comp not empty?)))

;;; Gender should be probably be a string... the prompt only mentions male and female,
;;; but both biological sex and gender are more complex.
;;; The ordering requested by the prompt is female, then male...
;;; this happens to be alphabetical ordering, so I'm going to allow any string,
;;; and sort alphabetically
;;;
;;; Alternately, I could just make gender #{:female male},
;;; call keyword on it when parsing, stringify when returning json via handlers, etc.
(spec/def ::gender (spec/and string? (comp not empty?)))


(spec/def ::favorite-color (spec/and string? (comp not empty?)))


(spec/def ::date-of-birth inst?)


(spec/def ::person (spec/keys :req-un [::last-name
                                       ::first-name
                                       ::gender
                                       ::favorite-color
                                       ::date-of-birth]))

;;;;;;;;;;;;;;;
;;; Parsing ;;;
;;;;;;;;;;;;;;;

;;; A quick note on the spec:

;;; The spec gives three examples of what lines might look like:
;;; - LastName | FirstName | Gender | FavoriteColor | DateOfBirth
;;; - LastName, FirstName, Gender, FavoriteColor, DateOfBirth
;;; - LastName FirstName Gender FavoriteColor DateOfBirth

;;; The spec also say that:
;;; > You may assume that the delimiters (commas, pipes and spaces) do not appear anywhere in the data values themselves.

;;; It's not clear how to interpret this, since spaces occur in all three examples.
;;; Does is mean that, in a |-delimited file, there will be no pipes in values, but there might be ','s or ' 's?
;;; Hard to tell.
;;; In particular, that would make a file with lines like:

;;; "Last,|First,|Gender,|Color,|DoB" ambiguous, since either of , or | could be the intended separator

;;; I'm going to interpret it as meaning that extra spaces might occur in any files,
;;; but the other delimiters will not occur in values anywhre,
;;; so I should treat extra spaces as whitespace in files that use the other delimiters.

;;; So, I'm considering a delimiter to be a string that looks like:
;;; - zero or more spaces
;;; - exactly one '|', ',', or ' '
;;; - zero or more spaces

;;; An alternative would be to require that the user specify the delimiter (to avoid the ambiguous delimiter situation above), but it's simple enough to use a single delimiter re that works for them all.

;;; All files and lines will be parsed with the same unified delimiter, as described above:
;;; zero or more spaces, then exactly on '|', ',', or ' ', then zero or more spaces
(def separator-re #" *[|, ] *")


(def dob-formatter (format/formatter "M/d/yyyy"))


(defn parse-line
  "Parse a single entry from a string of text representing a single line of an input file."
  [line]
  (let [[last first gender color dob-str] (clojure.string/split line separator-re)
        ;; I'm going to assume that dates will be input in the same
        ;; format in which they should be printed.
        dob (coerce/to-date (format/parse dob-formatter dob-str))]
    {:last-name  last
     :first-name first
     :gender     gender
     :color      color
     :dob        dob}))


(defn parse-string
  "Parse a string representing a collection of person records into a sequence of person maps."
  [s]
  (map parse-line (clojure.string/split-lines s)))


(defn parse-file
  "Parse a file representing a collection of person records into a sequence of person maps."
  [file-name]
  (parse-string (slurp (clojure.java.io/file file-name))))


(defn person-output-format
  "Convert a record to a printable format by converting date of birth to a human-friendly string."
  [person]
  (update person :dob (comp (partial format/unparse dob-formatter) coerce/from-date)))


;;; I don't like just printing the requested orderings without any
;;; headings to identify which ordering is which, so the user can
;;; easily see which ordering they're looking when the cli app prints
;;; all three.
;;;
;;; The most easily extensible idea is to just pass in a vector representing the orderings,
;;; something like: [[:last-name :desc] [:dob :asc]] for ordering by multiple fields,
;;; and use the field names to print nice headers when printing.
;;;
;;; Using that method, adding similar sorting options is trivial.
;;;
;;; A more flexible option, which generalizes to more ideas than just "order by this field",
;;; is to change the below sorting functions into little maps with fields like :header and :fn,
;;; where the :header is used for rendering the header and :fn can be any transformation.
;;;
;;; I have implemented neither of these things :P
;;;
;;; Actually I really should have done one of them instead of typing this comment explaining
;;; that I'm not doing either, since it has probably taken a comparable amount of time :P
;;;
;;; Ok you know what?  I'm just going to do it.  Now the comment won't make much sense anymore.
;;; I'll do the more-flexible, header + fn method.


;;; Back when this was just a function that provided the sorting,
;;; rather than a map with both a header string and a sorting function,
;;; this was named `gender-sorted`, because I think an adjective is the right part of speech for this.
;;; When returning the same kind of object, but with some different properties, adjectives are nice.
;;;
;;; But now it's a noun because it's an objecty map.
;;; (As opposed to, say, a functiony map)
;;;
;;; A gerund feels right to me, but I'm interested in your thoughts on the topic, gentle reviewer.
(def gender-sorting
  "Option 1 from the prompt: sort the records by gender, then by last name."
  {:header "Sorted by Gender"
   :fn     (fn [persons]
             (->> persons
                  ;; Sort is stable, so we can just sort repeatedly, with the primary ordering last.
                  (sort-by :last-name)
                  (sort-by :gender)))})


(def dob-sorting
  "Option 2 from the prompt: sort the records by date of birth, ascending (oldest first)."
  {:header "Sorted by Date of Birth"
   :fn (fn [persons]
         (sort-by :dob persons))})


(def last-name-sorting
  "Option 3 from the prompt: sort the records by last name, then first name."
  {:header "Sorder by Last Name(, then first name)"
   :fn (fn [persons]
         ;; If you're feeling clever, you can use juxt for ordering...
         ;; but if you wanted to reverse one of the orderings, this method will be a pain.
         ;; Probably better to use multiple sorts in that case, passing in a custom comp
         ;; to the three-argument arity.
         ;;
         ;; Ok I realize that the prompt does specify clarity over cleverness,
         ;; but I'm writing this for free in my spare time so I'm going to use juxt here :P
         (sort-by (juxt :last-name :first-name) persons))})


(defn sort-with
  "Sort `records` using the `:fn` in `sorting-map`."
  [sorting-map records]
  ((:fn sorting-map) records))
