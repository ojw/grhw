(ns grhw.core
  (:require [clojure.tools.cli :as tools.cli]
            [clojure.pprint :as pprint]

            [com.stuartsierra.component :as component]

            [grhw.person :as person]
            [grhw.db :as db]
            [grhw.server :as server])
  (:gen-class))


(defn log-records-to-console
  "For each fn in `ordering-fns`, pretty-prints the collection of person `records` to the console,
  sorted using ordering-fn."
  [records orderings]
  (doseq [ordering orderings]
    (println (:header ordering))
    (doseq [record ((:fn ordering) records)]
      (pprint/pprint (person/person-output-format record)))
    (newline)))


;;; There are a few more command-line options than required for the prompt,
;;; but I don't get to write command-line interfaces often so I figured I would throw them in.
(def cli-options
  [["-a" "--all" "Include all orderings"]
   ["-g" "--gender" "Sort by gender"]
   ["-d" "--dob" "Sort by date of birth"]
   ["-n" "--name" "Sort by name"]

   ["-s" "--server" "Start a web server"]
   ["-p" "--port PORT" "Run web server on port PORT"
    :default 8000
    :parse-fn #(Integer/parseInt %)]

   ["-h" "--help"]])


(defn usage
  "A string describing how to use this tool from the command line."
  [summary]
  (println "grhw, a tool for doing... something... with person records.")
  (newline)
  (println "Usage: lein run -- [options] file(s)")
  (newline)
  (println "Options:")
  (println summary)
  (println "Example: `lein run -- resources/test-files/simple.space`"))


(defn parse-opts
  "Parse command line options.  Returns a map suitable for consumption in -main."
  [args]
  (let [{:keys [options arguments summary] :as o}      (tools.cli/parse-opts args cli-options)
        {:keys [all gender dob name server help port]} options

        ;; If the user selects multiple command line options, they only get one,
        ;; based on the ordering below.
        ;; Handling multiple inputs is doable but added more noise than I wanted, so I'm opting
        ;; for clarity at the expense of our hypothetical user.
        orderings (cond
                    all        [person/gender-sorting
                                person/dob-sorting
                                person/last-name-sorting]
                    gender     [person/gender-sorting]
                    dob        [person/dob-sorting]
                    name       [person/last-name-sorting]
                    :otherwise [person/gender-sorting
                                person/dob-sorting
                                person/last-name-sorting])]
    {:files     arguments
     :orderings orderings
     :server?   server
     :summary   summary
     :help?     help
     :port      port}))


(defn new-system
  [port]
  (component/system-map
   :db (db/new-db)
   :server (component/using
            (server/new-web-server port)
            {:db :db})))


(defn -main
  "Parse `files` and print the resulting collection.
  Or maybe start a web server.
  It all comes down to the args!
  (see cli-options)"
  [& args]
  (let [{:keys [files orderings server? help? summary port]
         :as   opts} (parse-opts args)]
    (cond
      help?
      (println (usage summary))

      server?
      (let [system (component/start (new-system port))]
        (println "Server running on port " port)
        (println "RET to exit")
        (read-line)
        (component/stop system))

      (empty? files)
      (do (println "Either specify --server or include file name arguments.")
          (println (usage summary)))

      :otherwise
      (let [db (component/start (db/new-db))]
        (doseq [file files]
          (doseq [record (person/parse-file file)]
            (db/add-record db record)))
        (log-records-to-console  (db/records db) orderings)))))
