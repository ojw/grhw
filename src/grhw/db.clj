(ns grhw.db
  (:require [com.stuartsierra.component :as component]))


(defrecord Db [db]
  component/Lifecycle
  (start [this] this)
  (stop [this] (reset! db nil)))


(defn new-db []
  (map->Db {:db (atom [])}))


;;; An architectural extension to this pattern is to use a protocol to represent the API for a db.
;;; That way, other (sierra) components in a system can call API functions
;;; without knowing the underlying implementation.
;;; Basic OOP stuff.
;;; But here it's more than we need, and it'll be easy to switch add-record and records to protocols
;;; if the need ever arises.
(defn add-record
  "Add a record to database.
  Returns the new state of the db."
  [db record]
  (swap! (:db db) conj record))


(defn records
  "Returns all records in db."
  [db]
  @(:db db))
