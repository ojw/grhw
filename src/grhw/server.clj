(ns grhw.server
  (:require [clojure.data.json :as json]

            [org.httpkit.server :as server]
            [bidi.bidi :as bidi]
            [compojure.core :as compojure]
            [com.stuartsierra.component :as component]
            [ring.util.request :as request]

            [grhw.person :as person]
            [grhw.db :as db]))


(defn add-record
  "Handle a request to add a new record to the database."
  [req]
  (let [{:keys [db]} req
        body-string (request/body-string req)
        person (person/parse-line body-string)]
    (db/add-record db person)
    {:status 200
     ;; It would probably be more useful to return the json representation of the added entity.
     :body "Success, probably!"}))


(defn gender-sorted-records
  "Return a response with all records sorted by gender."
  [req]
  (let [{:keys [db]} req
        records      (db/records db)]
    {:status 200
     :body   (->> records
                  (person/sort-with person/gender-sorting)
                  (map person/person-output-format)
                  json/write-str)}))


(defn dob-sorted-records
  "Return a response with all records sorted by date of birth."
  [req]
  (let [{:keys [db]} req
        records      (db/records db)]
    {:status 200
     :body   (->> records
                  (person/sort-with person/dob-sorting)
                  (map person/person-output-format)
                  json/write-str)}))


(defn last-name-sorted-records
  "Return a response with all records sorted by last name, then first name."
  [req]
  (let [{:keys [db]} req
        records      (db/records db)]
    {:status 200
     :body   (->> records
                  (person/sort-with person/last-name-sorting)
                  (map person/person-output-format)
                  json/write-str)}))


;;; Usually I use something with bidirectional routing, like bidi
;;; (although I've been thinking about trying reitit lately)
;;; since that makes both endpoing testing and client-side development more convenient.
;;;
;;; But, I didn't want to bother making the additional architectural decisions that entails :P
(compojure/defroutes routes
  (compojure/context "/records" []
                     (compojure/POST "/" req (add-record req))
                     (compojure/GET "/gender" req (gender-sorted-records req))
                     (compojure/GET "/birthdate" req (dob-sorted-records req))
                     (compojure/GET "/name" req (last-name-sorted-records req))))


;;; We're going to give the handlers access to the running system's database this way.
;;; Classic ring stuff.
(defn wrap-db
  "Provide request handlers with access to the database."
  [handler db]
  (fn [request]
    (handler (assoc request :db db))))


;;; We're a ring app, and no ring app is complete without a totally baffling stack of middleware.
;;; What does each one do?  What happens if you reorder them?
;;; Good luck sorting it out!
;;; Hopefully nothing middleware related ever goes wrong.
(defn app
  "Wrap middleware around routes to create a runnable app."
  [db]
  (-> routes
      (wrap-db db)))


;;; Basic component pattern stuff.
;;; A WebServer here doesn't have any API beyond component/start and component/stop.
(defrecord WebServer [port db]
  component/Lifecycle
  (start [this]
    (let [stop (server/run-server (app db) {:port port})]
      (assoc this :stop stop)))
  (stop [this]
    ((:stop this))
    (dissoc this :stop)))


;;; As recommended by the sierra-component docs, a friendly little constructor.
(defn new-web-server
  "Create a WebServer that will listen on port `port`.
  The full API is `component/start` and `component/stop`."
  [port]
  (map->WebServer {:port port
                   :stop (atom nil)}))
