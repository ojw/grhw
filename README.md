# grhw

It does something with person records, for some reason.

I've just been using `lein run`, so that's the guidance I'm providing here:

`lein run -- -h` will provide usage information, which I'll just reproduce to save you a bit of time:

```
grhw, a tool for doing... something... with person records.

Usage: lein run -- [options] file(s)

Options:
  -a, --all              Include all orderings
  -g, --gender           Sort by gender
  -d, --dob              Sort by date of birth
  -n, --name             Sort by name
  -s, --server           Start a web server
  -p, --port PORT  8000  Run web server on port PORT
  -h, --help
```

To run the tests, use `lein test`.

To run the server and test using curl, try something like:

`lein run --server`

`curl http://localhost:8000/records/gender`

> $> []

`curl -X POST -d "last first female color 01/01/2011" http://localhost:8000/records`

> $> Success, probably!

`curl http://localhost:8000/records/gender`

> $> [{"last-name":"last","first-name":"first","gender":"female","color":"color","dob":"1\/1\/2011"}]



## License

Copyright © 2019 ojw

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
