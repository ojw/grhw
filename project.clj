(defproject grhw "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [clj-time "0.15.0"]
                 [http-kit "2.3.0"]
                 [org.clojure/tools.cli "0.4.2"]
                 [compojure "1.6.1"]
                 [bidi "2.1.6"]
                 [clj-http "3.10.0"]
                 [org.clojure/data.json "0.2.6"]
                 [com.stuartsierra/component "0.4.0"]]

  :plugins [[cider/cider-nrepl "0.21.1"]]
  :main ^:skip-aot grhw.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:dependencies [[org.clojure/test.check "0.9.0"]]}})
