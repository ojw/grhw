(ns grhw.core-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as spec]
            [clojure.spec.gen.alpha :as spec.gen]

            [clojure.data.json :as json]
            [clj-time.format :as format]
            [clj-time.coerce :as coerce]
            [com.stuartsierra.component :as component]
            [clj-http.client :as client]

            [grhw.core :refer :all]
            [grhw.db :as db]
            [grhw.person :as person]))

;;; Ok, the tests aren't great.
;;; Do they constitute 80% coverage?
;;; They cover... at least some of 80% of the important concepts, probably.
;;; They should use fixtures, and be split into namespaces.

(defn n-random-records
  [n]
  (spec.gen/sample (spec/gen ::person/person) n))

(deftest parsing-test
  (testing "Parsing a comma-delimited file returns the expected number of results."
    (is (= 3 (count (person/parse-file "resources/test-files/simple.comma")))))
  (testing "Parsing a pipe-delimited file returns the expected number of results."
    (is (= 3 (count (person/parse-file "resources/test-files/simple.pipe")))))
  (testing "Parsing a space-delimited file returns the expected number of results."
    (is (= 4 (count (person/parse-file "resources/test-files/simple.space"))))))

(deftest db-test
  (testing "Adding some records to an empty database results in the same number of records in the db."
    (let [db      (component/start (db/new-db))
          ;; I don't get to play with spec enough, or generative testing enough, so I'm shoehorning it in here (in some form).
          records (n-random-records 10)]
      (doseq [record records]
        (db/add-record db record))
      (is (= (count records)
             (count (db/records db)))))))

(defn sorted-by?
  "Is coll sorted by key-fn?
  Generates `number-to-check` random pairs of elements and checks that the corresponding elements
  are relatively ordered correctly."
  [coll key-fn number-to-check]
  (let [coll-count (count coll)
        pairs      (repeatedly number-to-check (fn [] [(rand-int coll-count) (rand-int coll-count)]))]
   (not (some (fn [[index-1 index-2]]
                (and (<= index-1 index-2)
                     (pos? (compare (key-fn (nth coll index-1))
                                    (key-fn (nth coll index-2))))))
              pairs))))

(deftest sorting-test
  (let [records-to-generate 100
        pairs-to-sample     10
        records             (n-random-records records-to-generate)
        gender-sorted       (person/sort-with person/gender-sorting records)
        dob-sorted          (person/sort-with person/dob-sorting records)
        last-name-sorted    (person/sort-with person/last-name-sorting records)]

    ;; Ok look, I realize that clojure has quick-check testing via test.check.
    ;; But, I've already spent way too long deciding how to write this thing,
    ;; so I'm just kinda rolling my own bad version.

    ;; I would really like to spend some time getting familiar with spec + test.check...
    ;; I think property-based testing is usually better than writing example-based tests, at least.

    ;; "There shouldn't be any pair of records where record A comes before record B in the ordering,
    ;; but record A's gender is greater than record B's gender."
    (testing "Gender-sorted records are correctly sorted."
      (is (sorted-by? gender-sorted :gender 10)))
    (testing "Dob-sorted records are correctly sorted."
      (is (sorted-by? dob-sorted :dob 10)))
    (testing "Last-name-sorted records are correctly sorted."
      (is (sorted-by? last-name-sorted :last-name 10)))))

(defn random-record-line
  []
  (let [last-name  (spec.gen/generate (spec/gen ::person/last-name))
        first-name (spec.gen/generate (spec/gen ::person/first-name))
        gender     (spec.gen/generate (spec/gen #{"female" "male"}))
        color      (spec.gen/generate (spec/gen ::person/favorite-color))
        inst       (java.util.Date.)
        dob        (format/unparse person/dob-formatter (coerce/from-date inst))
        separator  (spec.gen/generate (spec/gen #{"," "|" " "}))]
    (clojure.string/join separator [last-name first-name gender color dob])))

;;; Actually spin up a server and test the endpoints.
(deftest integration-test
  (let [{:keys [db server] :as system} (component/start (new-system 8123))
        api-root                       "http://localhost:8123/records"]
    (try
      (testing "Initially the database is empty."
        (is (empty? (db/records db))))
      (testing "Posting a record returns a happy response."
        (is (= 200 (:status (client/post api-root {:body "Orr James Male Fuchsia 07/28/1985"})))))
      (testing "The posted record is added to the database."
        (is (seq (db/records db))))
      (testing "The endpoints return reasonable results.")

      ;; First, let's load up a bunch of records...
      (dotimes [i 100]
        (client/post api-root {:body (random-record-line)}))
      (testing "Gender-sorted endpoint returns records gender-sorted."
        (let [result (json/read-json (:body (client/get (str api-root "/gender")))
                                     :keywordize-please)]
          (is (sorted-by? result :gender 20))))
      (testing "Last-name-sorted endpoint returns records last-name-sorted."
        (let [result (json/read-json (:body (client/get (str api-root "/name")))
                                     :keywordize-please)]
          (is (sorted-by? result :last-name 20))))
      (testing "Date-of-birth-sorted endpoint returns records date-of-birth-sorted."
        (let [result (json/read-json (:body (client/get (str api-root "/birthdate")))
                                     :keywordize-please)]
          (is (sorted-by? result (fn [json-record]
                                   ;; gotta re-parse these guys since the json is a month-first string
                                   (format/parse person/dob-formatter (:dob json-record))) 20))))
      (finally (component/stop system)))))
